/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   funnel.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/23 00:51:06 by garm              #+#    #+#             */
/*   Updated: 2014/12/26 08:13:43 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FUNNEL_H
# define FUNNEL_H

# include <stdio.h>
# include "libft.h"
# include "libftsock.h"

# define HOST_SIZE 128

typedef enum			s_cmd
{
	CMD_ERROR = ERROR,
	CMD_TUNNEL = 0,
	CMD_FUNNEL_SERVER,
	CMD_FUNNEL_CLIENT,
	CMD_REVERSE_FUNNEL_SERVER,
	CMD_REVERSE_FUNNEL_CLIENT,
	CMD_SIZE
}						t_cmd;

typedef struct			s_remote
{
	char				host[HOST_SIZE];
	int					port;

}						t_remote;

typedef struct			s_parsefun
{
	t_cmd				cmd;
	int					port;
	t_remote			to;
	t_remote			via;
	t_bool				is_daemon;
}						t_parsefun;

typedef struct			s_tunnel
{
	t_port				priv;
	t_remote			remote;
}						t_tunnel;

typedef struct			s_comtunnel
{
	t_com				priv;
	struct s_comtunnel	*link;
}						t_comtunnel;

/*
** parser.c
*/
void					funnel_parser(t_parsefun *p, int argc, char **argv);

#endif
