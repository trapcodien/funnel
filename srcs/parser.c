/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/24 00:07:07 by garm              #+#    #+#             */
/*   Updated: 2014/12/24 00:07:45 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "funnel.h"

static void		funnel_parse_cmd(t_parsefun *parser, char *command)
{
	if (3 == ft_sscanf(command, "listen %i to %s:%i",
						&parser->port, parser->to.host, &parser->to.port))
		parser->cmd = CMD_TUNNEL;
	if (1 == ft_sscanf(command, "listen %i", &parser->port))
		parser->cmd = CMD_FUNNEL_SERVER;
	if (5 == ft_sscanf(command, "listen %i to %s:%i via %s:%i",
						&parser->port, parser->to.host, &parser->to.port,
						parser->via.host, &parser->via.port))
		parser->cmd = CMD_FUNNEL_CLIENT;
	if (1 == ft_sscanf(command, "reverse listen %i", &parser->port))
		parser->cmd = CMD_REVERSE_FUNNEL_SERVER;
	if (2 == ft_sscanf(command, "reverse %s:%i",
						parser->to.host, &parser->to.port))
		parser->cmd = CMD_REVERSE_FUNNEL_CLIENT;
}

void			funnel_parser(t_parsefun *parser, int argc, char **argv)
{
	char	*command;

	parser->cmd = CMD_ERROR;
	if (argc <= 1)
		return ;
	if (!ft_strcmp(argv[1], "-d"))
	{
		argc--;
		argv++;
		parser->is_daemon = TRUE;
	}
	if (argc >= 2)
	{
		command = ft_tabmerge(++argv, --argc);
		funnel_parse_cmd(parser, command);
		ft_strdel(&command);
	}
}
