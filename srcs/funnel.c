/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   funnel.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/23 00:51:44 by garm              #+#    #+#             */
/*   Updated: 2014/12/29 23:06:21 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "funnel.h"

int			ft_usage(void)
{
	ft_fprintf(2, "Usages : \n[-d] : Daemon Mode.\n\n"
	"funnel [-d] listen <port> to <ip>:<port> ---> Basic TCP Tunnel.\n"
	"funnel [-d] listen <port> ---> Funnel Server.\n"
	"funnel [-d] listen <port> to <ip>:<port> "
										"via <ip>:<port> --> Funnel Client.\n\n"
	"funnel [-d] reverse listen <port> ---> Reverse Funnel Server.\n"
	"funnel [-d] reverse <ip>:<port> ---> Reverse Funnel Client.\n\n"
	"funnel (funnel help) ---> Displaye the funnel help.\n"
	"funnel list ---> Display the funnel list.\n"
	"funnel close <id_funnel> ---> Close a Funnel Tunnel.\n\n");
	return (1);
}

t_tunnel	*ft_create_tunnel(void)
{
	return ((t_tunnel *)ft_memalloc(sizeof(t_tunnel)));
}

t_comtunnel	*ft_create_comtunnel(void)
{
	return ((t_comtunnel *)ft_memalloc(sizeof(t_comtunnel)));
}

void		event_disconnect_tunnel(t_comtunnel *c)
{
	t_comtunnel		*temp;

	if ((temp = c->link))
	{
		c->link->link = NULL;
		c->link = NULL;
		network_close(&temp->priv);
	}
}

size_t		event_recv_tunnel(t_comtunnel *c, t_cbuff *cbuf, size_t size)
{
	t_com	*target;

	if (c->link)
	{
		target = &c->link->priv;
		cbuff_write_noflush(target->tcps->sock, cbuf, size);
	}
	return (size);
}

void		event_ack(t_com *c, void *data, size_t size)
{
	ft_memdel(&data);
	(void)c;
	(void)size;
}

void		event_connect_tunnel(t_comtunnel *client)
{
	t_net			*net;
	char			*host;
	int				port;
	t_tunnel		*tunnel;
	t_comtunnel		*c;

	tunnel = (t_tunnel *)client->priv.port;
	net = client->priv.port->net;
	host = tunnel->remote.host;
	port = tunnel->remote.port;
	disconnect_hook(&client->priv, &event_disconnect_tunnel);
	ack_hook(&client->priv, &event_ack);
	c = (t_comtunnel *)network_connect(net, host, port, &ft_create_comtunnel);
	if (c)
	{
		c->link = client;
		client->link = c;
		recv_hook(&c->priv, &event_recv_tunnel);
		recv_hook(&client->priv, &event_recv_tunnel);
		disconnect_hook(&c->priv, &event_disconnect_tunnel);
		ack_hook(&c->priv, &event_ack);
	}
	else
		network_close(&client->priv);
}

int			ft_basic_tunnel(t_parsefun *parse)
{
	t_net		*net;
	t_tunnel	*tunnel;
	int			port;

	if (!parse)
		return (1);
	port = parse->port;
	net = network_create(NULL);
	tunnel = (t_tunnel *)network_listen(net, port, 42, &ft_create_tunnel);
	if (!tunnel)
		return (ft_error("Basic tunnel : unable to bind port."));
	ft_putendl_fd("Basic tunnel : ok.", STDIN_FILENO);
	tunnel->remote = parse->to;
	connect_hook(&tunnel->priv, &event_connect_tunnel, &ft_create_comtunnel);
	network_loop(net);
	return (0);
}

int			main(int argc, char **argv)
{
	t_parsefun	parse;

	funnel_parser(&parse, argc, argv);
	if (parse.cmd == ERROR)
		return (ft_usage());
	if (parse.cmd == CMD_TUNNEL)
		return (ft_basic_tunnel(&parse));
	return (0);
}
