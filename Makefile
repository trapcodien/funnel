# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/08/26 23:49:31 by garm              #+#    #+#              #
##   Updated: 2014/12/29 23:07:26 by garm             ###   ########.fr       ##
#                                                                              #
# **************************************************************************** #

CC = gcc

NAME = funnel
LIBS = -lft -lftsock
LIBS_DIR = libft libftsock

SOURCES_DIR = srcs
INCLUDES_DIR = includes

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra
	CC = cc
else
	FLAGS = -Wall -Werror -Wextra -ansi -std=c89
endif

CFLAGS = $(foreach DIR, $(LIBS_DIR), -I ./$(DIR)/$(INCLUDES_DIR) )
CFLAGS += $(FLAGS) -I $(INCLUDES_DIR)

LDFLAGS = $(foreach DIR, $(LIBS_DIR), -L ./$(DIR) )
LDFLAGS += $(LIBS)


DEPENDENCIES = \
			   $(INCLUDES_DIR)/funnel.h

SOURCES = \
		  $(SOURCES_DIR)/funnel.c \
		  $(SOURCES_DIR)/parser.c \

OBJS = $(SOURCES:.c=.o)

all: $(NAME)

%.o: %.c $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS) lib
	@echo Creating $(NAME)...
	@$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

lib:
	@$(foreach dir, $(LIBS_DIR), make -C $(dir);)

clean:
	@$(foreach dir, $(LIBS_DIR), make clean -C $(dir);)
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

fclean: clean
	@$(foreach dir, $(LIBS_DIR), make cleanbin -C $(dir);)
	@rm -f $(NAME)
	@rm -rf $(NAME).dSYM
	@echo Deleting $(NAME)...

re: fclean all

.PHONY: lib clean fclean re all

