# FUNNEL #
**Funnel** is an easy tunneler program.
It allows you to create different TCP tunnel.
**Funnel** support reverse connection.

With **Funnel**, you can make 3 things : 

* **Basic TCP Tunnel**
* **Funnel Server/Client TCP Tunnel**
* **Reverse Funnel Server/Client TCP Tunnel**

A **Funnel TCP Tunnel** allows you to use one port for establish others connections with others ports...
For example : you can easier perform an 'IP Hijacking' attack.

Summary
=======
[Instructions](#markdown-header--instructions)
==================
* [Download](#markdown-header-download)
* [Compilation](#markdown-header-compilation)
* [Usage](#markdown-header-usage)

#**       - Instructions          **#
## Download ##
```
#!shell
git clone https://trapcodien@bitbucket.org/trapcodien/funnel.git
cd funnel
git submodule init
git submodule update
```

## Compilation ##
```
#!shell
make funnel
```

## Usage ##

[-d] : Daemon Mode.

* **funnel [-d] listen <port> to <ip>:<port>** ---> Basic TCP Tunnel.
* **funnel [-d] listen <port>** ---> Funnel Server.
* **funnel [-d] listen <port> to <ip>:<port>  via <ip>:<port>** ---> Funnel Client.

* **funnel [-d] reverse listen <port>** ---> Reverse Funnel Server.
* **funnel [-d] reverse <ip>:<port>** ---> Reverse Funnel Client.

* **funnel (funnel help)** ---> Displaye the funnel help.
* **funnel list** ---> Display the funnel list.
* **funnel close <id_funnel>** ---> Close a Funnel Tunnel.

